package com.blog.personal.bean;

import com.blog.personal.dto.UserDTO;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class AuthenticationResponse implements Serializable {

    private static final long serialVersionUID = -8863221651931782740L;

    private final String token;

    private final UserDTO userDTO;

    public AuthenticationResponse(String token, UserDTO userDTO) {
        this.token = token;
        this.userDTO = userDTO;
    }
}
