package com.blog.personal.bean;

import com.blog.personal.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class TrackingResponse implements Serializable {

    private UserDTO userDTO;

    private List numberVisits;

    private List numberVisitsWithNPM;

}
