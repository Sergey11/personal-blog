package com.blog.personal.bean;

import com.blog.personal.dto.PostDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class PostsResponse implements Serializable {

    private static final long serialVersionUID = -7292276386191820481L;
    private Integer page;
    private Long amountOfAllPosts;
    private Long totalPages;
    private List<PostDTO> postsDTOList;

}
