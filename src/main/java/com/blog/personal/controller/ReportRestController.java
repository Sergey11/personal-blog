package com.blog.personal.controller;

import com.blog.personal.bean.TrackingResponse;
import com.blog.personal.dto.UserDTO;
import com.blog.personal.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ReportRestController {

    @Autowired
    TrackingService trackingService;

    @Autowired
    private UserDetailsService userDetailsService;

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "reports", method = RequestMethod.GET)
    public ResponseEntity<?> getAuthenticatedUser() {
        String name = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        UserDTO user = (UserDTO) userDetailsService.loadUserByUsername(name);
        TrackingResponse response = trackingService.getNumberOfUniqueVisits(user.getUserId());
        return ResponseEntity.ok(response);
    }
}
