package com.blog.personal.controller;

import com.blog.personal.dto.UserDTO;
import com.blog.personal.service.UserService;
import com.blog.personal.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("classpath:jwt.properties")
@RequestMapping(value = "/api/v1")
public class UserRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public ResponseEntity<?> getAuthenticatedUser() {
        String userEmail = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        String username = jwtTokenUtil.getUsernameFromToken(userEmail);
        UserDTO user = (UserDTO) userDetailsService.loadUserByUsername(username);
        return ResponseEntity.ok(user);
    }


    @RequestMapping(value = {"/singup"}, method = RequestMethod.POST)
    public ResponseEntity<?> singUp(@RequestBody UserDTO userDTO) {
        userService.createUser(userDTO);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = {"/settings"}, method = RequestMethod.POST)
    public ResponseEntity<?> updateUserSettings(@RequestBody UserDTO userDTO) {
        String userEmail = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        userService.updateUserSettings(userDTO, userEmail);
        return ResponseEntity.ok().build();
    }

}