package com.blog.personal.controller;

import com.blog.personal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @Autowired
    private UserService userService;

    @RequestMapping({
            "/",
            "/*",
            "/post_editor/*",
            "/post/*"
    })
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @RequestMapping(value = "/email/confirmation/{key}", method = RequestMethod.GET)
    public String emailConfirmation(@PathVariable("key") String key) {
        if (userService.confirmEmail(key)) {
            return "redirect:/";
        } else {
            return "400";
        }
    }

}
