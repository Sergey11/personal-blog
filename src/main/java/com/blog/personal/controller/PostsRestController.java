package com.blog.personal.controller;

import com.blog.personal.bean.PostsResponse;
import com.blog.personal.dto.PostDTO;
import com.blog.personal.dto.UserDTO;
import com.blog.personal.service.PostService;
import com.blog.personal.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class PostsRestController {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PostService postService;

    @Autowired
    private TrackingService trackingService;

    @RequestMapping(value = "posts", params = {"page", "size"}, method = RequestMethod.GET)
    public ResponseEntity<?> findPaginatedPosts(@RequestParam("page") int pageNumber,
                                                @RequestParam("size") int pageSize) {
        PostsResponse postsResponse = postService.findPaginatedPosts(pageNumber, pageSize);
        if (pageNumber > postsResponse.getTotalPages()) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok().body(postsResponse);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "myposts", params = {"page", "size"}, method = RequestMethod.GET)
    public ResponseEntity<?> findMyPaginatedPosts(@RequestParam("page") int pageNumber,
                                                @RequestParam("size") int pageSize) {
        String name = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        UserDTO user = (UserDTO) userDetailsService.loadUserByUsername(name);
        PostsResponse postsResponse = postService.findMyPaginatedPosts(pageNumber, pageSize, user.getUserId());
        if (pageNumber > postsResponse.getTotalPages()) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok().body(postsResponse);
    }

    @RequestMapping(value = {"/post/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<?> getPost(@PathVariable("id") int id, @RequestParam Map<String, String> paramMap, HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request
                    .getRemoteAddr();
        }
        PostDTO postDTO = postService.getPostById(id);
        trackingService.trackPost(postDTO, ipAddress, paramMap);
        return ResponseEntity.ok().body(postDTO);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = {"/post/{id}"}, method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable("id") int id) {
        postService.deletePostById(id);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = {"/save_post"}, method = RequestMethod.POST)
    public ResponseEntity<?> saveOrUpdatePost(@RequestBody PostDTO postDTO) {
        String name = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        UserDTO user = (UserDTO) userDetailsService.loadUserByUsername(name);
        postService.createPosts(postDTO, user.getUserId());
        return ResponseEntity.ok().build();
    }
}
