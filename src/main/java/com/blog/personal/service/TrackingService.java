package com.blog.personal.service;

import com.blog.personal.bean.TrackingResponse;
import com.blog.personal.dto.PostDTO;

import java.util.Map;

public interface TrackingService {

    void trackPost(PostDTO postDTO, String ipAddress, Map<String, String> paramMap);

    TrackingResponse getNumberOfUniqueVisits(Long userId);
}