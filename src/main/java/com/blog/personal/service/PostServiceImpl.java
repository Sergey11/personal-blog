package com.blog.personal.service;

import com.blog.personal.bean.PostsResponse;
import com.blog.personal.dao.PostDAO;
import com.blog.personal.dao.TrackingDAO;
import com.blog.personal.dto.PostDTO;
import com.blog.personal.model.Post;
import com.blog.personal.model.User;
import com.blog.personal.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostDAO postDAO;

    @Autowired
    private TrackingDAO trackingDAO;

    @Autowired
    private Mapper mapper;

    @Override
    public void createPosts(PostDTO postDTO, long userId) {
        postDTO.setCreationDate(new Date());
        Post post = mapper.map(postDTO, Post.class);
        post.setUser(new User(userId));
        postDAO.createOrUpdatePost(post);
    }

    @Override
    public PostDTO getPostById(int id) {
        PostDTO postDTO = mapper.map(postDAO.getPostById(id), PostDTO.class);
        postDTO.setNumberOfUniqueVisits(trackingDAO.getAllNumberOfUniqueVisits((long) postDTO.getPostId()));
        return postDTO;
    }

    @Override
    public void deletePostById(int id) {
        postDAO.deletePostById(id);
    }

    public PostsResponse findPaginatedPosts(int pageNumber, int pageSize) {
        PostsResponse postsResponse = new PostsResponse();
        Long amount = postDAO.getAmountOfAllPosts();
        List<Post> postList = postDAO.findPaginatedPosts(pageNumber, pageSize, amount);
        postsResponse.setPage(pageNumber);
        postsResponse.setAmountOfAllPosts(amount);
        List<PostDTO> postDTOList = postList.stream()
                .map(p -> mapper.map(p, PostDTO.class))
                .collect(Collectors.toList());
        postsResponse.setPostsDTOList(postDTOList);
        postsResponse.setTotalPages(amount / pageSize);
        return postsResponse;
    }

    @Override
    public PostsResponse findMyPaginatedPosts(int pageNumber, int pageSize, Long userId) {
        PostsResponse postsResponse = new PostsResponse();
        Long amount = postDAO.getAmountOfMyPosts(userId);
        List<Post> postList = postDAO.findMyPaginatedPosts(pageNumber, pageSize, userId);
        postsResponse.setPage(pageNumber);
        postsResponse.setAmountOfAllPosts(amount);
        List<PostDTO> postDTOList = postList.stream()
                .map(p -> mapper.map(p, PostDTO.class))
                .collect(Collectors.toList());
        postsResponse.setPostsDTOList(postDTOList);
        postsResponse.setTotalPages(amount / pageSize);
        return postsResponse;
    }
}
