package com.blog.personal.service;

import com.blog.personal.dao.UserDAO;
import com.blog.personal.dto.Role;
import com.blog.personal.dto.UserDTO;
import com.blog.personal.model.User;
import com.blog.personal.model.UserRole;
import com.blog.personal.util.Mapper;
import com.blog.personal.util.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private Mapper mapper;

    @Override
    public void createUser(UserDTO userDTO) {
        userDTO.setAuthorities(new ArrayList<>());
        userDTO.setConfirmationKey(RandomUtil.getEmailConfirmationKey());
        userDTO.setEnabled(true);
        userDTO.setLocale("en");
        User user = mapper.map(userDTO, User.class);
        userDAO.saveOrUpdate(user);

        UserRole userRole = new UserRole(Role.ADMIN);
        userRole.setUser(user);
        userDAO.saveOrUpdate(userRole);
        emailSenderService.sentEmailConfirmation(userDTO);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDAO.getUserByEmail(email);
    }

    @Override
    public boolean confirmEmail(String confirmationKey) {
        User user = userDAO.getUserByConfirmationKey(confirmationKey);
        if (user != null && (user.getActive() == null || !user.getActive())) {
            user.setActive(true);
            userDAO.saveOrUpdate(user);
            return true;
        }
        return false;
    }

    @Override
    public void updateUserSettings(UserDTO userDTO, String userEmail) {
        User user = userDAO.getUserByEmail(userEmail);
        user.setPassword(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
        user.setLocale(userDTO.getLocale());
        user.setUsername(userDTO.getUsernameLM());
        user.setGender(userDTO.getGender());
        userDAO.saveOrUpdate(user);
    }
}
