package com.blog.personal.service;

import com.blog.personal.dto.UserDTO;
import com.blog.personal.model.User;

public interface UserService {

    void createUser(UserDTO userDTO);

    User getUserByEmail(String email);

    boolean confirmEmail(String confirmationKey);

    void updateUserSettings(UserDTO userDTO, String username);

}
