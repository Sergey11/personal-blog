package com.blog.personal.service;

import com.blog.personal.dto.UserDTO;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailSenderService {

    private static final String APPLICATION_URL = "http://127.0.0.1:8080";

    private static final String SENDER_ADDRESS = "youremail@gmail.com";

    public void sentEmailConfirmation(UserDTO user) {
        String subject = "Email confirmation";
        String content = "Please confirm your email \n"
                + "Click on link : " + APPLICATION_URL + "/email/confirmation/" + user.getConfirmationKey();
        sentEmail(user.getEmail(), subject, content);
    }

    public void sentEmail(String address, String subject, String content) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new EmailAuthenticator());

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SENDER_ADDRESS));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(address));
            message.setSubject(subject);
            message.setContent(content, "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Letter sent to : " + address);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}