package com.blog.personal.service;

import com.blog.personal.bean.TrackingResponse;
import com.blog.personal.dao.TrackingDAO;
import com.blog.personal.dto.PostDTO;
import com.blog.personal.model.Post;
import com.blog.personal.model.Tracking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TrackingServiceImpl implements TrackingService {

    @Autowired
    private TrackingDAO trackingDAO;

    @Override
    public void trackPost(PostDTO postDTO, String ipAddress, Map<String, String> paramMap) {

        Tracking newTracking = new Tracking();
        newTracking.setDatetime(new Date());
        newTracking.setIpAddress(ipAddress);
        newTracking.setPost(new Post(postDTO.getPostId()));
        newTracking.setUtmSource(paramMap.get("utm_source"));
        newTracking.setUtmMedium(paramMap.get("utm_medium"));
        newTracking.setUtmCampaign(paramMap.get("utm_campaign"));
        newTracking.setUtmTerm(paramMap.get("utm_term"));
        newTracking.setUtmContent(paramMap.get("utm_content"));

        Tracking tracking = trackingDAO.getTrackingByIp(ipAddress);
        if (tracking != null) {
            newTracking.setTrackId(tracking.getTrackId());
        }
        trackingDAO.trackPost(newTracking);
    }

    @Override
    public TrackingResponse getNumberOfUniqueVisits(Long userId) {
        List numberVisits = trackingDAO.getNumberOfUniqueVisits(userId);
        List numberVisitsWithNPM = trackingDAO.getNumberOfUniqueVisitsWithNPM(userId);
        TrackingResponse trackingResponse = new TrackingResponse();
        trackingResponse.setNumberVisits(numberVisits);
        trackingResponse.setNumberVisitsWithNPM(numberVisitsWithNPM);
        return trackingResponse;
    }
}