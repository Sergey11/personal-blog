package com.blog.personal.service;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class EmailAuthenticator extends Authenticator {

    //TODO: Insert login/password for your email
    private final String USERNAME = "myemail@gmail.com";
    private final String PASSWORD = "mypassword";

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(USERNAME, PASSWORD);
    }
}
