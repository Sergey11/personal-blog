package com.blog.personal.service;

import com.blog.personal.bean.PostsResponse;
import com.blog.personal.dto.PostDTO;

public interface PostService {

    void createPosts(PostDTO postDTO, long userId);

    PostDTO getPostById(int id);

    void deletePostById(int id);

    PostsResponse findPaginatedPosts(int pageNumber, int pageSize);

    PostsResponse findMyPaginatedPosts(int pageNumber, int pageSize, Long userId);

}
