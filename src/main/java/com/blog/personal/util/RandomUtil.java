package com.blog.personal.util;

import java.util.UUID;

public class RandomUtil {
    public static String getEmailConfirmationKey() {
        return UUID.randomUUID().toString();
    }
}