package com.blog.personal.util;

import com.blog.personal.dto.Role;
import com.blog.personal.dto.UserDTO;
import com.blog.personal.model.User;
import com.blog.personal.model.UserRole;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component

public class Mapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(User.class, UserDTO.class)
                .customize(new CustomMapper<User, UserDTO>() {
                    @Override
                    public void mapAtoB(User user, UserDTO userDTO, MappingContext context) {
                        super.mapAtoB(user, userDTO, context);
                        userDTO.setUserId(user.getUserId());
                        userDTO.setUsernameLM(user.getUsername());
                        userDTO.setLocale(user.getLocale());
                        userDTO.setPassword(user.getPassword());
                        userDTO.setEnabled(user.getEnabled());
                        userDTO.setEmail(user.getEmail());
                        userDTO.setAge(user.getAge());
                        userDTO.setGender(user.getGender());
                        userDTO.setAuthorities(user.getUserRoles()
                                .stream()
                                .map(UserRole::getRole)
                                .collect(Collectors.toList()));
                        userDTO.setConfirmationKey(user.getConfirmationKey());
                        userDTO.setActive(user.getActive());
                        userDTO.setLastPasswordResetDate(user.getLastPasswordResetDate());
                    }

                    @Override
                    public void mapBtoA(UserDTO userDTO, User user, MappingContext context) {
                        super.mapBtoA(userDTO, user, context);
                        user.setUserId(userDTO.getUserId());
                        user.setUsername(userDTO.getUsername());
                        user.setLocale(userDTO.getLocale());
                        user.setPassword(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
                        user.setEnabled(userDTO.isEnabled());
                        user.setEmail(userDTO.getEmail());
                        user.setAge(userDTO.getAge());
                        user.setGender(userDTO.getGender());
                        user.setUserRoles(userDTO.getAuthorities()
                                .stream()
                                .map(u -> new UserRole(Role.ADMIN))
                                .collect(Collectors.toList()));
                        user.setConfirmationKey(userDTO.getConfirmationKey());
                        user.setActive(userDTO.isActive());
                    }
                })
                .register();

        super.configure(factory);
    }
}
