package com.blog.personal.dao;

import com.blog.personal.model.Post;

import java.util.List;

public interface PostDAO {

    void createOrUpdatePost(Post post);

    Post getPostById(int id);

    void deletePostById(int id);

    Long getAmountOfAllPosts();

    List<Post> findPaginatedPosts(int pageNumber, int pageSize, Long amountOfAllPosts);

    Long getAmountOfMyPosts(Long userId);

    List<Post> findMyPaginatedPosts(int pageNumber, int pageSize, Long userId);

}
