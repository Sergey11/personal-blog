package com.blog.personal.dao;

import com.blog.personal.model.User;
import com.blog.personal.model.UserRole;

public interface UserDAO {

    User getUserByEmail(String email);

    void saveOrUpdate(User user);

    void saveOrUpdate(UserRole user);

    User getUserByConfirmationKey(String confirmationKey);
}