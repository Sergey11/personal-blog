package com.blog.personal.dao;

import com.blog.personal.model.Post;
import com.blog.personal.model.Tracking;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
@Transactional
public class TrackingDAOImpl implements TrackingDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session openSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void trackPost(Tracking tracking) {
        openSession().saveOrUpdate(tracking);
    }

    @Override
    public Tracking getTrackingByIp(String ip) {
        CriteriaBuilder builder = openSession().getCriteriaBuilder();
        CriteriaQuery<Tracking> criteria = builder.createQuery(Tracking.class);
        Root<Tracking> root = criteria.from(Tracking.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("ipAddress"), ip));
        Tracking tracking = openSession().createQuery(criteria)
                .uniqueResult();
        return tracking;
    }

    @Override
    public List getNumberOfUniqueVisits(Long userId) {
        Query query = openSession().createNativeQuery(
                "select \n" +
                        "    tr.post_id as postId,\n" +
                        "    p.title as title,\n" +
                        "    sum(case when DAY(tr.datetime) = DAY(CURDATE()) then 1 else 0 end) as perTodayCount,\n" +
                        "    sum(case when WEEK(tr.datetime) = WEEK(CURDATE()) then 1 else 0 end) as thisWeekCount,\n" +
                        "    sum(case when MONTH(tr.datetime) = MONTH(CURDATE()) then 1 else 0 end) as thisMonthCount,\n" +
                        "    count(tr.post_id) as allCount\n" +
                        "from tracking as tr\n" +
                        "inner join posts as p\n" +
                        "ON tr.post_id=p.post_id\n" +
                        "inner join users as u\n" +
                        "ON p.user_id=u.user_id\n" +
                        "where u.user_id = :userId\n" +
                        "group by tr.post_id;");
        query.setParameter("userId", userId);

        List list = query.getResultList();
        return list;
    }

    @Override
    public List getNumberOfUniqueVisitsWithNPM(Long userId) {
        Query query = openSession().createNativeQuery(
                "select \n" +
                        "    tr.post_id as postId,\n" +
                        "    p.title as title,\n" +
                        "    tr.utm_source,\n" +
                        "    tr.utm_medium,\n" +
                        "    tr.utm_campaign,\n" +
                        "    tr.utm_term,\n" +
                        "    tr.utm_content,\n" +
                        "    sum(case when DAY(tr.datetime) = DAY(CURDATE()) then 1 else 0 end) perTodayCount,\n" +
                        "    sum(case when WEEK(tr.datetime) = WEEK(CURDATE()) then 1 else 0 end) thisWeekCount,\n" +
                        "    sum(case when MONTH(tr.datetime) = MONTH(CURDATE()) then 1 else 0 end) thisMonthCount,\n" +
                        "    count(tr.post_id) allCount \n" +
                        "from tracking as tr\n" +
                        "inner join posts as p\n" +
                        "ON tr.post_id = p.post_id\n" +
                        "inner join users as u\n" +
                        "ON p.user_id = u.user_id\n" +
                        "where u.user_id = :userId and (tr.utm_source IS NOT NULL OR tr.utm_medium IS NOT NULL OR tr.utm_campaign IS NOT NULL OR tr.utm_term IS NOT NULL OR tr.utm_content IS NOT NULL )\n" +
                        "group by tr.post_id, tr.utm_source, tr.utm_medium, tr.utm_campaign, tr.utm_term, tr.utm_content;");
        query.setParameter("userId", userId);
        List list = query.getResultList();
        return list;
    }

    @Override
    public Long getAllNumberOfUniqueVisits(Long postId) {
        CriteriaBuilder criteriaBuilder = openSession().getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<Post> postRoot = countQuery.from(Post.class);
        Join<Post, Tracking> join = postRoot.join("trackingList", JoinType.INNER);
        countQuery.where(criteriaBuilder.equal(postRoot.get("postId"), postId));
        countQuery.select(criteriaBuilder.count(join));
        Long amount = openSession().createQuery(countQuery).getSingleResult();
        return amount;
    }
}