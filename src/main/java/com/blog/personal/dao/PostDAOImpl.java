package com.blog.personal.dao;

import com.blog.personal.model.Post;
import com.blog.personal.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
@Transactional
public class PostDAOImpl implements PostDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session openSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Long getAmountOfAllPosts() {
        CriteriaBuilder criteriaBuilder = openSession().getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        countQuery.select(criteriaBuilder.count(countQuery.from(Post.class)));
        Long amount = openSession().createQuery(countQuery).getSingleResult();
        return amount;
    }

    @Override
    public List<Post> findPaginatedPosts(int pageNumber, int pageSize, Long amountOfAllPosts) {
        CriteriaBuilder criteriaBuilder = openSession().getCriteriaBuilder();

        CriteriaQuery<Post> criteriaQuery = criteriaBuilder.createQuery(Post.class);
        Root<Post> from = criteriaQuery.from(Post.class);
        criteriaQuery.select(from);
        CriteriaQuery<Post> select = criteriaQuery.orderBy(criteriaBuilder.asc(from.get("creationDate")));

        TypedQuery<Post> typedQuery = openSession().createQuery(select);
        int firstResult = pageNumber == 1 ? 0 : (pageNumber - 1) * pageSize;
        typedQuery.setFirstResult(firstResult);
        typedQuery.setMaxResults(pageSize);
        List<Post> postList = typedQuery.getResultList();
        return postList;
    }

    @Override
    public Long getAmountOfMyPosts(Long userId) {
        CriteriaBuilder criteriaBuilder = openSession().getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<Post> postRoot = countQuery.from(Post.class);
        Join<Post, User> join = postRoot.join("user", JoinType.INNER);
        join.on(criteriaBuilder.equal(join.get("userId"), userId));
        countQuery.select(criteriaBuilder.count(postRoot));
        Long amount = openSession().createQuery(countQuery).getSingleResult();
        return amount;
    }

    @Override
    public List<Post> findMyPaginatedPosts(int pageNumber, int pageSize, Long userId) {
        CriteriaBuilder criteriaBuilder = openSession().getCriteriaBuilder();

        CriteriaQuery<Post> criteriaQuery = criteriaBuilder.createQuery(Post.class);
        Root<Post> postRoot = criteriaQuery.from(Post.class);
        Join<Post, User> join = postRoot.join("user", JoinType.INNER);
        join.on(criteriaBuilder.equal(join.get("userId"), userId));
        criteriaQuery.select(postRoot);
        CriteriaQuery<Post> select = criteriaQuery.orderBy(criteriaBuilder.asc(postRoot.get("creationDate")));

        TypedQuery<Post> typedQuery = openSession().createQuery(select);
        int firstResult = pageNumber == 1 ? 0 : (pageNumber - 1) * pageSize;
        typedQuery.setFirstResult(firstResult);
        typedQuery.setMaxResults(pageSize);
        List<Post> postList = typedQuery.getResultList();
        return postList;
    }

    @Override
    public void createOrUpdatePost(Post post) {
        openSession().saveOrUpdate(post);
    }

    @Override
    public Post getPostById(int id) {
        Post post = openSession().get(Post.class, id);
        return post;
    }

    @Override
    public void deletePostById(int postId) {
        openSession().delete(getPostById(postId));
    }
}
