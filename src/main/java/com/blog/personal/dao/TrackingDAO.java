package com.blog.personal.dao;

import com.blog.personal.model.Tracking;

import java.util.List;

public interface TrackingDAO {

    void trackPost(Tracking tracking);

    Tracking getTrackingByIp(String ip);

    List getNumberOfUniqueVisits(Long userId);

    List getNumberOfUniqueVisitsWithNPM(Long userId);

    Long getAllNumberOfUniqueVisits(Long postId);
}