package com.blog.personal.dao;

import com.blog.personal.model.User;
import com.blog.personal.model.UserRole;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session openSession() {
        return sessionFactory.getCurrentSession();
    }

    public User getUserByEmail(String email) {
        CriteriaBuilder builder = openSession().getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("email"), email));
        User user = openSession().createQuery(criteria)
                .uniqueResult();
        return user;
    }

    @Override
    public void saveOrUpdate(User user) {
        openSession().saveOrUpdate(user);
    }

    @Override
    public void saveOrUpdate(UserRole userRole) {
        openSession().saveOrUpdate(userRole);
    }

    @Override
    public User getUserByConfirmationKey(String confirmationKey) {
        CriteriaBuilder builder = openSession().getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("confirmationKey"), confirmationKey));
        User user = openSession().createQuery(criteria)
                .uniqueResult();
        return user;
    }
}
