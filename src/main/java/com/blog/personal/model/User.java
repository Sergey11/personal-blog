package com.blog.personal.model;

import com.blog.personal.dto.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = -3920193636234790714L;

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId = 0L;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "username")
    private String username;

    @Column(name = "age")
    private Integer age;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "locale")
    private String locale;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "confirmation_key")
    private String confirmationKey;

    @Column(name = "last_password_reset_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordResetDate;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<UserRole> userRoles = new ArrayList<>();

    public User() {
    }

    public User(Long userId) {
        this.userId = userId;
    }
}