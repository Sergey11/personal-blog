package com.blog.personal.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

@Getter
@Setter
public class UserDTO implements UserDetails {

    private static final long serialVersionUID = -3217036007023129188L;

    @JsonIgnore
    private long userId;
    private String usernameLM;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String locale;
    private boolean enabled;
    private boolean active;
    private String email;
    private Integer age;
    private Gender gender;
    private Collection<? extends GrantedAuthority> authorities;
    private String confirmationKey;
    @JsonIgnore
    private Date lastPasswordResetDate;

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
