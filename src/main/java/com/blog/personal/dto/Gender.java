package com.blog.personal.dto;

public enum Gender {
    DEFAULT("default"),
    MALE("male"),
    FEMALE("female");

    private String identifier;

    Gender(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}