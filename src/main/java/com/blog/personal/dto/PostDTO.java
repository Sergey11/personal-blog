package com.blog.personal.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class PostDTO implements Serializable {

    private static final long serialVersionUID = 67243192087726331L;

    private int postId;
    private String title;
    private String body;
    private Date creationDate;
    private UserDTO user;
    private Long numberOfUniqueVisits;

}