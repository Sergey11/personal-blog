package com.blog.personal.dto;

import com.blog.personal.model.Post;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
class TrackingDTO implements Serializable {

    private static final long serialVersionUID = 1743018037128124181L;

    private int trackId;

    private String ipAddress;

    private Date datetime;

    private String utmSource;

    private String utmMedium;

    private String utmCampaign;

    private String utmTerm;

    private String utmContent;

    private Post post;
}