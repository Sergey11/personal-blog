<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container" ng-controller="Reports as rep">
    <div class="row">
        <div class="col-md-12">
            <legend>{{'reports.numberVisits' | translate}}</legend>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>{{'reports.table.postId' | translate}}</th>
                        <th>{{'reports.table.title' | translate}}</th>
                        <th>{{'reports.table.perToday' | translate}}</th>
                        <th>{{'reports.table.thisWeek' | translate}}</th>
                        <th>{{'reports.table.thisMonth' | translate}}</th>
                        <th>{{'reports.table.all' | translate}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="numVis in rep.numberVisits">
                        <td>{{numVis[0]}}</td>
                        <td>{{numVis[1]}}</td>
                        <td>{{numVis[2]}}</td>
                        <td>{{numVis[3]}}</td>
                        <td>{{numVis[4]}}</td>
                        <td>{{numVis[5]}}</td>
                        <td>
                            <div class="col-md-2">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="clearfix"></div>
            </div>

            <legend>{{'reports.numberVisitsWithNPM' | translate}}</legend>
            <div class="table-responsive">
                <table id="numberVisitsWithNPM" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>{{'reports.table.postId' | translate}}</th>
                        <th>{{'reports.table.title' | translate}}</th>
                        <th>{{'reports.table.utmSource' | translate}}</th>
                        <th>{{'reports.table.utmMedium' | translate}}</th>
                        <th>{{'reports.table.utmCampaign' | translate}}</th>
                        <th>{{'reports.table.utmTerm' | translate}}</th>
                        <th>{{'reports.table.utmContent' | translate}}</th>
                        <th>{{'reports.table.perToday' | translate}}</th>
                        <th>{{'reports.table.thisWeek' | translate}}</th>
                        <th>{{'reports.table.thisMonth' | translate}}</th>
                        <th>{{'reports.table.all' | translate}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="numVisNPM in rep.numberVisitsWithNPM">
                        <td>{{numVisNPM[0]}}</td>
                        <td>{{numVisNPM[1]}}</td>
                        <td>{{numVisNPM[2]}}</td>
                        <td>{{numVisNPM[3]}}</td>
                        <td>{{numVisNPM[4]}}</td>
                        <td>{{numVisNPM[5]}}</td>
                        <td>{{numVisNPM[6]}}</td>
                        <td>{{numVisNPM[7]}}</td>
                        <td>{{numVisNPM[8]}}</td>
                        <td>{{numVisNPM[9]}}</td>
                        <td>{{numVisNPM[10]}}</td>
                        <td>
                            <div class="col-md-2">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>