</div>
<h3>{{'posts.allPostsTitle' | translate}}</h3>
</div>

<div ng-repeat="blogPost in filteredPosts">
    <h4>{{'posts.creationDate' | translate}} {{blogPost.creationDate | date}}</h4>
    <p>{{'posts.author' | translate}} {{blogPost.user.usernameLM}}</p>
    <h4>{{'posts.title' | translate}} {{blogPost.title}}</h4>
    <p>{{blogPost.body}}</p>
    <a href="/post/{{blogPost.postId}}">View details &raquo;</a>
    <hr/>
</div>
<div>
    <pagination ng-show="amountOfAllPosts > numPerPage"
                ng-model="currentPage"
                total-items="totalPages"
                max-size="maxSize"
                boundary-links="true">
    </pagination>
</div>