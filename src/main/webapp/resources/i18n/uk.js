angular.module('indexApp').config(function ($translateProvider) {
    $translateProvider.translations('uk', {
        menu: {
            login: 'Ввійти',
            singUp: 'Зареєструватись',
            admin: 'Адміністратор',
            logout: 'Вийти'
        },
        login: {
            title: 'Ввійти',
            email: {
                label: 'Електронна пошта',
                placeholder: 'Введіть адресу вашої електронної пошти'
            },
            password: {
                label: 'Пароль',
                placeholder: 'Введіть ваш пароль'
            }
        },
        button: {
            login: 'Увійти',
            save: 'Зберегти',
            submit: 'Відправити'
        },
        footer: {
            link: {
                about: 'Про сайт'
            }
        },

        about: {
            text: 'Автор Сергій Мусіенко'
        },
        posts: {
            myPostsTitle: 'Мої повідомлення',
            allPostsTitle: 'Всі повідомлення',
            creationDate: 'Дата створення:',
            author: 'Автор:',
            title: 'Назва:',
            details: 'Докладніше'
        },
        admin: {
            myPosts: {
                title: 'Мої повідомлення',
                desc: 'Список моїх власних блогів',
                btn: 'Перейти до моїх Повідомленнь'
            },
            settings: {
                title: 'Налаштування',
                desc: 'Параметри сторінки, де можна побачити і управляти своїми власними налаштуваннями облікового запису',
                btn: 'Перейти на сторінку Налаштування'
            },
            reports: {
                title: 'Звіт',
                desc: 'Таблиці зі статистикою для ваших повідомлень',
                btn: 'Перейти на сторінку Звіти'
            }
        },

        reports: {
            numberVisits: 'Кількість унікальних відвідувань для кожного повідомлення',
            numberVisitsWithNPM: 'Кількість унікальних відвідувань кожного повідомлення для кожної комбінації UTM Params',
            table: {
                postId: 'id',
                title: 'назва',
                perToday: 'за сьогодні',
                thisWeek: 'цього тижня',
                thisMonth: 'цього місяця',
                all: 'за весь час',
                utmSource: 'utm source',
                utmMedium: 'tm medium',
                utmCampaign: 'utm campaign',
                utmTerm: 'utm term',
                utmContent: 'utm content'
            }
        },

        postEditor: {
            mainTitle: 'Повідомлення',
            title: 'Назва :',
            body: 'Текст :'
        },
        settings: {
            mainTitle: 'Налаштування профілю',
            newPassword: {
                label: 'Новий пароль',
                placeholder: 'Введіть новий пароль'
            },
            locale: {
                label: 'Локаль',
                en: 'Англійська',
                uk: 'Український'
            },
            username: {
                label: 'Ім\'я та прізвище',
                placeholder: 'Введіть ім\'я та прізвище'
            },
            gender: {
                label: 'Стать',
                default: 'Не визначено',
                male: 'Чоловіча',
                female: 'Жіноча'
            }
        },
        singUp: {
            mainTitle: 'Зареєструватися',
            email: {
                label: 'Електронна пошта',
                placeholder: 'Введіть адресу електронної пошти'
            },
            password: {
                label: 'Пароль',
                placeholder: 'Введіть пароль'
            },
            username: {
                label: 'Ім\'я та прізвище',
                placeholder: 'Введіть своє ім\'я та прізвище'
            },
            gender: {
                label: 'Стать',
                default: 'Не визначено',
                male: 'Чоловіча',
                female: 'Жіноча'
            },
            age: {
                label: 'Вік',
                placeholder: 'Введіть свій вік'
            },
            registered: 'Вже зареєстровані ?',
            loginHere: 'Вхід тут'
        }
    });
});