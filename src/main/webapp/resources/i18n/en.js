angular.module('indexApp').config(function ($translateProvider) {
    $translateProvider.translations('en', {
        menu: {
            login: 'Login',
            singUp: 'Sing Up',
            admin: 'Admin',
            logout: 'Logout'
        },
        login: {
            title: 'Login',
            email: {
                label: 'Email',
                placeholder: 'Enter your email'
            },
            password: {
                label: 'Password',
                placeholder: 'Enter your password'
            }
        },
        button: {
            login: 'Login',
            save: 'Save',
            submit: 'Submit'
        },
        footer: {
            link: {
                about: 'About site'
            }
        },
        about: {
            text: 'Author Serhii Musienko'
        },
        posts: {
            myPostsTitle: 'My posts',
            allPostsTitle: 'All posts',
            creationDate: 'Creation date:',
            author: 'Author:',
            title: 'Title:',
            details: 'View details'
        },

        admin: {
            myPosts: {
                title: 'My posts',
                desc: 'List of my own blogs',
                btn: 'Go to My Posts pages'
            },
            settings: {
                title: 'Settings',
                desc: 'Settings Page where you can see and manage his own account settings',
                btn: 'Go to Settings page'
            },
            reports: {
                title: 'Reports',
                desc: 'Table with statistics for your posts',
                btn: 'Go to Reports page'
            }
        },

        reports: {
            numberVisits: 'Number of unique visits for each post',
            numberVisitsWithNPM: 'Number of unique visits for each post for each combination of utm params',
            table: {
                postId: 'post id',
                title: 'title',
                perToday: 'per today',
                thisWeek: 'this week',
                thisMonth: 'this month',
                all: 'all',
                utmSource: 'utm source',
                utmMedium: 'utm medium',
                utmCampaign: 'utm campaign',
                utmTerm: 'utm term',
                utmContent: 'utm content'
            }
        },

        postEditor: {
            mainTitle: 'My Post',
            title: 'Title :',
            body: 'Body :'
        },

        settings: {
            mainTitle: 'Account setting',
            newPassword: {
                label: 'New password',
                placeholder: 'Enter new password'
            },
            locale: {
                label: 'Locale',
                en: 'English',
                uk: 'Ukrainian'
            },
            username: {
                label: 'First and Last name',
                placeholder: 'Enter First and Last name'
            },
            gender: {
                label: 'Gender',
                default: 'Not specified',
                male: 'Male',
                female: 'Female'
            }
        },

        singUp: {
            mainTitle: 'Sign Up',
            email: {
                label: 'Email',
                placeholder: 'Enter your email'
            },
            password: {
                label: 'Password',
                placeholder: 'Enter your password'
            },
            username: {
                label: 'First and Last name',
                placeholder: 'Enter your First and Last name'
            },
            gender: {
                label: 'Gender',
                default: 'Not specified',
                male: 'Male',
                female: 'Female'
            },
            age: {
                label: 'Age',
                placeholder: 'Enter your age'
            },
            registered: 'Already Registered ?',
            loginHere: 'Login here'
        }
    });
});