<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container" ng-controller="PostEditorCtrl as pe">
    <div class="page-header">
        <h3>{{'postEditor.mainTitle' | translate}}</h3>
    </div>
    <form>
        <div class="form-group">
            <label for="title">{{'postEditor.title' | translate}}</label>
            <input id="title" class="form-control" type="text" ng-maxlength="255" ng-model="pe.title">
        </div>
        <div class="form-group">
            <label for="textbody">{{'postEditor.body' | translate}}</label>
            <textarea id="textbody" class="form-control" rows="5" maxlength="65025" ng-model="pe.body"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" ng-click="pe.save()"><span class="fa fa-check"></span>{{'button.save' | translate}}
            </button>
        </div>
    </form>
</div>