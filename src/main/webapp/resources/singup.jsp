<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container" ng-controller="SingUp as sgn">
    <legend>{{'singUp.mainTitle' | translate}}</legend>

    <form class="form-signin form-horizontal" ng-submit="sgn.singUp()">
        <div class="form-group">
            <label class="col-md-4 control-label required" for="email">{{'singUp.email.label' | translate}}</label>
            <div class="col-md-4">
                <input id="email" name="email" type="email" placeholder="{{'singUp.email.placeholder' | translate}}"
                       class="form-control input-md" required ng-model="sgn.email">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label required" for="passwordinput">{{'singUp.password.label' | translate}}</label>
            <div class="col-md-4">
                <input id="passwordinput" name="passwordinput" type="password" placeholder="{{'singUp.password.placeholder' | translate}}"
                       class="form-control input-md" required ng-model="sgn.password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="username">{{'singUp.username.label' | translate}}</label>
            <div class="col-md-4">
                <input id="username" name="username" type="text" placeholder="{{'singUp.username.placeholder' | translate}}"
                       class="form-control input-md" ng-maxlength="100" ng-model="sgn.usernameLM">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="gender">{{'singUp.gender.label' | translate}}</label>
            <div class="col-md-4">
                <select id="gender" name="gender" class="form-control" ng-model="sgn.gender"
                        ng-init="sgn.gender='DEFAULT'">
                    <option value="DEFAULT">{{'singUp.gender.default' | translate}}</option>
                    <option value="MALE">{{'singUp.gender.male' | translate}}</option>
                    <option value="FEMALE">{{'singUp.gender.female' | translate}}</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="age">{{'singUp.age.label' | translate}}</label>
            <div class="col-md-4">
                <input id="age" name="age" type="text" placeholder="{{'singUp.age.placeholder' | translate}}"
                       class="form-control input-md" min="1" max="200" ng-model="sgn.age">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <input id="submit" name="submit" type="submit" value="{{'button.submit' | translate}}" class="btn btn-primary"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">
                {{'singUp.registered' | translate}}<a href="/login"> {{'singUp.loginHere' | translate}}</a>
            </div>
        </div>
    </form>

</div>