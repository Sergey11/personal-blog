<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container" ng-controller="Setting as setting">
    <form class="form-horizontal" ng-submit="setting.saveSettings()">
        <fieldset>

            <legend>{{'settings.mainTitle' | translate}}</legend>

            <div class="form-group">
                <label class="col-md-4 control-label" for="passwordinput">{{'settings.newPassword.label' | translate}}</label>
                <div class="col-md-4">
                    <input id="passwordinput" name="passwordinput" type="password" placeholder="{{'settings.newPassword.placeholder' | translate}}"
                           class="form-control input-md" required ng-model="setting.password">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="locale">{{'settings.locale.label' | translate}}</label>
                <div class="col-md-4">
                    <select id="locale" name="locale" class="form-control" ng-model="setting.locale"
                            ng-init="setting.locale='en'">
                        <option value="en">{{'settings.locale.en' | translate}}</option>
                        <option value="uk">{{'settings.locale.uk' | translate}}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="username">{{'settings.username.label' | translate}}</label>
                <div class="col-md-4">
                    <input id="username" name="username" type="text" placeholder="{{'settings.username.placeholder' | translate}}"
                           class="form-control input-md" ng-model="setting.username">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="gender" ng-model="setting.gender"
                       ng-init="setting.gender='DEFAULT'">{{'settings.gender.label' | translate}}</label>
                <div class="col-md-4">
                    <select id="gender" name="gender" class="form-control">
                        <option value="default">{{'settings.gender.default' | translate}}</option>
                        <option value="male">{{'settings.gender.male' | translate}}</option>
                        <option value="female">{{'settings.gender.female' | translate}}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="save"></label>
                <div class="col-md-4">
                    <input id="save" name="save" type="submit" value="{{'button.save' | translate}}" class="btn btn-primary"/>
                </div>
            </div>

        </fieldset>
    </form>

</div>