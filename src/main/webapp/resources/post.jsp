<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="jumbotron">
    <div class="container" ng-controller="PostDetails as post">
        <div class="row">
            <div class="col-md-7">
                <h2>{{post.title}}</h2>
                <p>{{post.body}}</p>
            </div>
            <div class="col-md-3"><span class="pull-right">
                <h2>{{post.username}}</h2>
            </span>
            </div>
            <div class="col-md-2"><span class="pull-right"><h2>{{post.numberOfUniqueVisits}} <span class="glyphicon glyphicon-eye-open"></span></h2></span>
            </div>
        </div>
    </div>
</div>
