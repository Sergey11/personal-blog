angular.module('indexApp', ['ngRoute', 'ngCookies', 'pascalprecht.translate', 'ui.bootstrap'])
    .factory('authInterceptor', authInterceptor)
    .service('user', userService)
    .service('auth', authService)
    .service('posts', postsService)
    .service('myposts', mypostsService)
    .service('settings', settingsService)
    .service('reports', reportsService)
    .constant('API', '/api/v1')
    .constant('ADMIN_URL_LIST', ['/admin', '/settings', '/reports', '/post_editor'])
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    })
    .controller('Main', MainCtrl)
    .controller('SingUp', SingUpController)
    .controller('Login', LoginController)
    .controller('PostEditorCtrl', PostEditorController)
    .controller('Setting', SettingsController)
    .controller('Posts', PostsController)
    .controller('MyPosts', MyPostsController)
    .controller('PostDetails', PostDetailsController)
    .controller('Reports', ReportsController)
    .config(function ($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/', {templateUrl: 'resources/allposts.jsp', controller: PostsController})
            .when('/login', {templateUrl: 'resources/login.jsp'})
            .when('/singup', {templateUrl: 'resources/singup.jsp'})
            .when('/myposts', {templateUrl: 'resources/myposts.jsp', controller: MyPostsController})
            .when('/settings', {templateUrl: 'resources/settings.jsp', controller: SettingsController})
            .when('/reports', {templateUrl: 'resources/reports.jsp', controller: ReportsController})
            .when('/admin', {templateUrl: 'resources/admin.jsp'})
            .when('/post_editor/:id', {templateUrl: 'resources/posteditor.jsp', controller: PostEditorController})
            .when('/post_editor', {templateUrl: 'resources/posteditor.jsp', controller: PostEditorController})
            .when('/post/:id', {templateUrl: 'resources/post.jsp', controller: PostDetailsController})
            .when('/about', {templateUrl: 'resources/about.jsp'})
            .otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode(true);
    })
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy('escaped');
    }]).run(function ($rootScope, $translate, auth, $location, ADMIN_URL_LIST) {
        $rootScope.$on('$viewContentLoaded', function () {
            $rootScope.currentPath = $location.path();
            var userLocale = auth.userLocale();
            if (userLocale) {
                $translate.use(userLocale);
            } else {
                $translate.use('en');
            }
        });
        $rootScope.$on('$routeChangeStart', function (event) {
            if (!auth.isAuthed('ADMIN') && ADMIN_URL_LIST.indexOf($location.path()) > -1) {
                event.preventDefault();
                $location.path('/');
            }
        });
        $rootScope.initialized = true;
    });

function authInterceptor($injector, API, auth) {
    return {
        request: function (config) {
            var token = auth.getToken();
            if (config.url.indexOf(API) === 0 && token) {
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        },

        response: function (res) {
            if (res.config.url.indexOf(API) === 0 && res.data.token) {
                auth.saveToken(res.data.token);
                auth.saveUser(res.data.userDTO);
            }
            return res;
        }
    }
}

function authService($window, $translate, $location) {
    var srvc = this;

    srvc.parseJwt = function (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse($window.atob(base64));
    };

    srvc.saveToken = function (token) {
        $window.localStorage['jwtToken'] = token
    };

    srvc.saveUser = function (user) {
        $window.localStorage['activeUser'] = user.active;
        $window.localStorage['userRole'] = user.authorities;
        $window.localStorage['userLocale'] = user.locale;
    };

    srvc.isActiveUser = function () {
        return $window.localStorage['activeUser'];
    };

    srvc.userRole = function () {
        return $window.localStorage['userRole'];
    };

    srvc.userLocale = function () {
        return $window.localStorage['userLocale'];
    };

    srvc.logout = function (token) {
        $window.localStorage.removeItem('jwtToken');
        $window.localStorage.removeItem('activeUser');
        $window.localStorage.removeItem('userRole');
        $window.localStorage.removeItem('userLocale');
        $location.path("/login");
    };

    srvc.getToken = function () {
        return $window.localStorage['jwtToken'];
    };

    srvc.isAuthed = function (roles) {
        var token = srvc.getToken();
        var userRole = srvc.userRole();
        var userLocale = srvc.userLocale();
        if (token) {
            $translate.use(userLocale);
            var params = srvc.parseJwt(token);
            return Math.round(new Date().getTime() / 1000) <= params.exp && roles.indexOf(userRole) !== -1;
        } else {
            return false;
        }
    };
}

function userService($http, API, auth) {
    var srvc = this;
    srvc.getUser = function () {
        return $http.get(API + '/user')
    };
    srvc.singUp = function (email, password, username, gender, age) {
        return $http.post(API + '/auth/singup', {
            email: email,
            password: password,
            username: username,
            gender: gender,
            age: age
        });
    };

    srvc.login = function (email, password) {
        return $http.post(API + '/auth/login', {
            email: email,
            password: password
        });
    };
}

function postsService($http, API) {
    var srvc = this;
    srvc.getPosts = function (page, size) {
        return $http.get(API + '/posts', {
            params: {page: page, size: size}
        });
    };

    srvc.savePost = function (tite, body) {
        return $http.post(API + '/save_post', {
            postId: 0,
            title: tite,
            body: body
        });
    };

    srvc.getPostByID = function (postId) {
        return $http.get(API + '/post/' + postId);
    };

    srvc.deletePost = function (postId) {
        return $http.delete(API + '/post/' + postId);
    };

    srvc.getPostDetails = function (postId) {
        return $http.get(API + '/post/' + postId);
    };
}

function mypostsService($http, API) {
    var srvc = this;
    srvc.getPosts = function (page, size) {
        return $http.get(API + '/myposts', {
            params: {page: page, size: size}
        });
    };

    srvc.savePost = function (tite, body) {
        return $http.post(API + '/save_post', {
            postId: 0,
            title: tite,
            body: body
        });
    };

    srvc.getPostByID = function (postId) {
        return $http.get(API + '/post/' + postId);
    };

    srvc.deletePost = function (postId) {
        return $http.delete(API + '/post/' + postId);
    };

    srvc.getPostDetails = function (postId) {
        return $http.get(API + '/post/' + postId);
    };
}

function settingsService($http, API) {
    var srvc = this;

    srvc.saveSettings = function (password, locale, username, gender) {
        return $http.post(API + '/settings', {
            password: password,
            locale: locale,
            usernameLM: username,
            gender: gender
        });
    }
}

function reportsService($http, API) {
    var srvc = this;

    srvc.getReports = function () {
        return $http.get(API + '/reports');
    }
}

function MainCtrl($scope, $translate, user, auth) {

    $scope.changeLanguage = function (lang) {
        $translate.use(lang);
    };

    var self = this;

    function handleRequest(res) {
        var token = res.data ? res.data.token : null;
        if (token) {
            console.log('JWT:', token);
        }
        self.message = res.data.message;
    }

    self.login = function () {
        user.login(self.email, self.password)
            .then(handleRequest, handleRequest)
    };
    self.singUp = function () {
        user.singUp(self.email, self.password)
            .then(handleRequest, handleRequest)
    };
    self.getUser = function () {
        user.getUser()
            .then(handleRequest, handleRequest)
    };
    self.logout = function () {
        auth.logout && auth.logout()
    };
    self.isAuthed = function (roles) {
        return auth.isAuthed(roles) ? auth.isAuthed(roles) : false
    };
    self.isActiveUser = function () {
        if (auth.isActiveUser() && self.isAuthed('ADMIN')) {
            return (auth.isActiveUser() === 'true') && self.isAuthed('ADMIN')
        }
        return false
    };
}

function LoginController($scope, $rootScope, $location, user) {
    var self = this;
    $rootScope.errorMessage = "";
    $rootScope.successMessage = "";

    function handleSuccessRequest(res) {
        var token = res.data ? res.data.token : null;
        if (token) {
            console.log('JWT:', token);
        }
        $location.path('/');
    }

    function handleErrorRequest(res) {
        $rootScope.errorMessage = "Your email or password invalid";
    }

    self.login = function () {
        user.login(self.email, self.password)
            .then(handleSuccessRequest, handleErrorRequest)
    };
}

function SingUpController($location, user) {
    var self = this;

    function handleRequest(res) {
        $location.path('/');
    }

    function handleErrorRequest(res) {
    }

    self.singUp = function () {
        user.singUp(self.email, self.password, self.username, self.gender, self.age)
            .then(handleRequest, handleErrorRequest)
    };
}

function LogoutController(auth) {
    auth.logout();
}

function MypostsController(auth) {
}

function SettingsController($window, $location, $translate, settings) {
    var self = this;

    function handleSuccessRequest(res) {
        $window.localStorage['userLocale'] = self.locale;
        $location.path("/");
    }

    function handleErrorRequest(res) {

    }

    self.saveSettings = function () {
        settings.saveSettings(self.password, self.locale, self.username, self.gender)
            .then(handleSuccessRequest, handleErrorRequest);
    };
}

function ReportsController($location, reports) {
    var self = this;

    function handleSuccessRequest(res) {
        self.numberVisits = res.data.numberVisits;
        self.numberVisitsWithNPM = res.data.numberVisitsWithNPM;
    }

    function handleErrorRequest(res) {
        $location.path("/");
    }

    self.getReports = function () {
        reports.getReports()
            .then(handleSuccessRequest, handleErrorRequest);
    };

    self.getReports();
}

function PostEditorController($location, posts, $routeParams) {
    var self = this;
    self.title = "";

    if ($routeParams.id) {
        posts.getPostByID($routeParams.id)
            .then(handleGetPostsSuccessRequest, handleGetPostsErrorRequest);
    }

    function handleGetPostsSuccessRequest(res) {
        if (res.data) {
            self.title = res.data.title;
            self.body = res.data.body;
        }
    }

    function handleGetPostsErrorRequest(res) {
    }

    self.save = function () {
        posts.savePost(self.title, self.body)
            .then(handleSaveSuccessRequest, handleSaveErrorRequest);
    };

    function handleSaveSuccessRequest(res) {
        $location.path("/myposts");
    }

    function handleSaveErrorRequest(res) {
    }
}

function PostsController($scope, posts) {
    var self = this;

    $scope.filteredPosts = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 5;
    $scope.maxSize = 5;

    function handleSuccessRequest(res) {
        var posts = res.data ? res.data : null;
        if (posts) {
            $scope.filteredPosts = posts.postsDTOList;
            $scope.amountOfAllPosts = posts.amountOfAllPosts;
            $scope.totalPages = posts.totalPages * 10 + 1;
        }
    }

    function handleErrorRequest(res) {
        var token = res.data ? res.data.token : null;
        if (token) {
        }
        self.message = res.data.message;
    }

    self.getPosts = function () {
        posts.getPosts($scope.currentPage, $scope.numPerPage)
            .then(handleSuccessRequest, handleErrorRequest)
    };

    $scope.$watch('currentPage + numPerPage', function () {
        self.getPosts();
    });
}

function MyPostsController($scope, $route, myposts) {
    var self = this;

    $scope.filteredPosts = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 5;
    $scope.maxSize = 5;

    function handleSuccessRequest(res) {
        var posts = res.data ? res.data : null;
        if (posts) {
            $scope.filteredPosts = posts.postsDTOList;
            $scope.amountOfAllPosts = posts.amountOfAllPosts;
            $scope.totalPages = posts.totalPages * 10 + 1;
        }
    }

    function handleErrorRequest(res) {
        var token = res.data ? res.data.token : null;
        if (token) {
        }
        self.message = res.data.message;
    }

    self.getMyPosts = function () {
        myposts.getPosts($scope.currentPage, $scope.numPerPage)
            .then(handleSuccessRequest, handleErrorRequest)
    };

    self.deletePost = function (postId) {
        myposts.deletePost(postId)
            .then(handleDeleteSuccessRequest, handleDeleteErrorRequest)
    };

    function handleDeleteSuccessRequest() {
        $route.reload();
    }

    function handleDeleteErrorRequest() {
        $route.reload();
    }

    $scope.$watch('currentPage + numPerPage', function () {
        self.getMyPosts();
    });
}

function PostDetailsController($routeParams, posts) {
    var self = this;

    if ($routeParams.id) {
        posts.getPostByID($routeParams.id)
            .then(handleSuccessRequest, handleErrorRequest);
    }

    function handleSuccessRequest(res) {
        var post = res.data ? res.data : null;
        if (post) {
            self.title = post.title;
            self.body = post.body;
            self.username = post.user.usernameLM;
            self.numberOfUniqueVisits = post.numberOfUniqueVisits;
        }
    }

    function handleErrorRequest(res) {
    }
}