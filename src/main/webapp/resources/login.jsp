<div class="container" ng-controller="Login as log">
    <form class="form-horizontal" ng-submit="log.login()">
        <fieldset>

            <legend>{{'login.title' | translate}}</legend>
            <div class="alert alert-danger" ng-show="errorMessage">
                <div class="text-center"><strong>{{errorMessage}}</strong></div>
            </div>
            <div class="alert alert-danger" ng-show="successMessage">
                <div class="text-center"><strong>{{successMessage}}</strong></div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">{{'login.email.label' | translate}}</label>
                <div class="col-md-4">
                    <input id="email" name="email" type="email"
                           placeholder="{{'login.email.placeholder' | translate}}"
                           class="form-control input-md" required ng-model="log.email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="passwordinput">{{'login.password.label' | translate}}</label>
                <div class="col-md-4">
                    <input id="passwordinput" name="passwordinput" type="password"
                           placeholder="{{'login.password.placeholder' | translate}}"
                           class="form-control input-md" required ng-model="log.password">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <input id="submit" name="submit" type="submit" value="{{'button.login' | translate}}"
                           class="btn btn-primary"/>
                </div>
            </div>

        </fieldset>
    </form>
</div>