<div class="page-header" ng-controller="Main as main" >
    <div class="pull-right" ng-show="main.isActiveUser()">
        <a href="/post_editor" class="btn btn-default" title="Create">
            <span class="fa fa-plus"></span>
        </a>
    </div>
    <h3>{{'posts.myPostsTitle' | translate}}</h3>
</div>
<div ng-repeat="blogPost in filteredPosts" ng-controller="MyPosts as posts">
    <div class="pull-right">
        <button class="btn btn-xs btn-default" ng-click="posts.deletePost(blogPost.postId)"><span
                class="fa fa-trash-o fa-2x"></span></button>
        <a href="/post_editor/{{blogPost.postId}}" class="btn btn-xs btn-default" ng-show="!main.isAuthed(['Admin'])"
           title="edit"><span class="fa fa-pencil fa-2x"></span></a>
    </div>
    <h4>{{'posts.creationDate' | translate}} {{blogPost.creationDate | date}}</h4>
    <p>{{'posts.author' | translate}} {{blogPost.user.usernameLM}}</p>
    <h4>{{'posts.title' | translate}} {{blogPost.title}}</h4>
    <p>{{blogPost.body}}</p>
    <a href="/post/{{blogPost.postId}}">{{'myPosts.details' | translate}} &raquo;</a>
    <hr/>
</div>
<div>
    <pagination ng-show="amountOfAllPosts > numPerPage"
                ng-model="currentPage"
                total-items="totalPages"
                max-size="maxSize"
                boundary-links="true">
    </pagination>
</div>