<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    <div class="col-sm-12">

        <div class="bs-calltoaction bs-calltoaction-info">
            <div class="row">
                <div class="col-md-9 cta-contents">
                    <h1 class="cta-title">{{'admin.myPosts.title' | translate}}</h1>
                    <div class="cta-desc">
                        <p>{{'admin.myPosts.desc' | translate}}</p>
                    </div>
                </div>
                <div class="col-md-3 cta-button">
                    <a href="myposts" class="btn btn-lg btn-block btn-info">{{'admin.myPosts.btn' | translate}}</a>
                </div>
            </div>
        </div>

        <div class="bs-calltoaction bs-calltoaction-success">
            <div class="row">
                <div class="col-md-9 cta-contents">
                    <h1 class="cta-title">{{'admin.settings.title' | translate}}</h1>
                    <div class="cta-desc">
                        <p>{{'admin.settings.desc' | translate}}</p>
                    </div>
                </div>
                <div class="col-md-3 cta-button">
                    <a href="settings" class="btn btn-lg btn-block btn-success">{{'admin.settings.btn' | translate}}</a>
                </div>
            </div>
        </div>

        <div class="bs-calltoaction bs-calltoaction-warning">
            <div class="row">
                <div class="col-md-9 cta-contents">
                    <h1 class="cta-title">{{'admin.reports.title' | translate}}</h1>
                    <div class="cta-desc">
                        <p>{{'admin.reports.desc' | translate}}</p>
                    </div>
                </div>
                <div class="col-md-3 cta-button">
                    <a href="reports" class="btn btn-lg btn-block btn-warning">{{'admin.reports.btn' | translate}}</a>
                </div>
            </div>
        </div>

    </div>
</div>