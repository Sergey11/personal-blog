<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="indexApp">
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-hQpvDQiCJaD2H465dQfA717v7lu5qHWtDbWNPvaTJ0ID5xnPUlVXnKzq7b8YUkbN" crossorigin="anonymous"/>
    <link href="/resources/css/admin.css" rel="stylesheet"/>
    <link href="/resources/css/main.css" rel="stylesheet"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-resource.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-route.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-cookies.min.js"></script>

    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-touch.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.min.js"></script>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.1/angular-sanitize.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.13.1/angular-translate.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.13.1/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.13.1/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.13.1/angular-translate-handler-log/angular-translate-handler-log.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.13.1/angular-translate-loader-url/angular-translate-loader-url.min.js"></script>

    <script data-require="ui-bootstrap@*" data-semver="0.12.1"
            src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

    <script src="/resources/js/main.js"></script>
    <script src="/resources/i18n/en.js"></script>
    <script src="/resources/i18n/uk.js"></script>
</head>
<body>
<div class="main wrap">
    <div class="container-fluid" ng-controller="Main as main">
        <div class="row">
            <div class="col-md-10 col-sm-offset-1">
                <nav class="navbar navbar-default" role="navigation" ng-show="initialized">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" ng-href="/">Logo ))</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li ng-show="!main.isAuthed(['ADMIN'])" ng-class="{active: currentPath === '/login'}">
                                <a ng-href="login">{{'menu.login' | translate}}</a>
                            </li>

                            <li ng-show="!main.isAuthed(['ADMIN'])" ng-class="{active: currentPath === '/singup'}">
                                <a ng-href="singup">{{'menu.singUp' | translate}}</a>
                            </li>
                            <li ng-show="main.isAuthed(['ADMIN'])" ng-class="{active: currentPath === '/admin'}">
                                <a ng-href="admin">{{'menu.admin' | translate}}</a>
                            </li>
                            <li ng-show="main.isAuthed(['ADMIN'])" ng-class="{active: currentPath === '/logout'}">
                                <a href="" ng-click="main.logout()">{{'menu.logout' | translate}}</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="container" ng-view></div>
</div>

<footer class="footer">
    <p class="text-muted credit col-md-10 col-sm-offset-2">
        <a href="about">{{'footer.link.about' | translate}}</a>
    </p>
</footer>
<base href="/"/>


</body>
</html>
