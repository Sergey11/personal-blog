CREATE SCHEMA IF NOT EXISTS personal_blog
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE users (
  user_id                  INT          NOT NULL AUTO_INCREMENT,
  email                    VARCHAR(64)  NOT NULL,
  password                 VARCHAR(100) NOT NULL,
  username                 VARCHAR(255),
  age                      INT,
  gender                   VARCHAR(60),
  locale                   VARCHAR(60),
  active                   BOOLEAN,
  enabled                  BOOLEAN,
  confirmation_key         VARCHAR(60),
  last_password_reset_date TIMESTAMP,
  PRIMARY KEY (user_id),
  UNIQUE KEY uni_email (email)
);


CREATE TABLE user_roles (
  role_id INT         NOT NULL AUTO_INCREMENT,
  role    VARCHAR(60) NOT NULL,
  user_id INT         NOT NULL,
  PRIMARY KEY (role_id),
  UNIQUE KEY uni_userid_role (user_id, role),
  CONSTRAINT fk_user_role_id FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE posts (
  post_id       INT NOT NULL AUTO_INCREMENT,
  title         VARCHAR(350),
  body          TEXT,
  creation_date DATETIME,
  user_id       INT,
  PRIMARY KEY (post_id),
  CONSTRAINT fk_post_user FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE tracking (
  track_id     INT NOT NULL AUTO_INCREMENT,
  ip_address   VARCHAR(255),
  datetime     DATETIME,
  utm_source   VARCHAR(255),
  utm_medium   VARCHAR(255),
  utm_campaign VARCHAR(255),
  utm_term     VARCHAR(255),
  utm_content  VARCHAR(255),
  post_id      INT,
  PRIMARY KEY (track_id),
  CONSTRAINT fk_tracking_post FOREIGN KEY (post_id) REFERENCES posts (post_id)
);

CREATE INDEX idx_user_email
  ON users (email);

CREATE INDEX idx_tracking_ip
  ON tracking (ip_address);